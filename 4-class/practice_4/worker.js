// TODO 17: 在这里写实现代码
import Student from './student';

class Worker extends Student {
  constructor(name, age, classname) {
    super(name, age, classname);
  }

  introduce() {
    let result = super.basic_introduce() + ' I am a Worker. I have a job.';
    return result;
  }
}

export default Worker;
