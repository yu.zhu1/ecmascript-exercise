// TODO 9: 在这里写实现代码
class Person {
  constructor(name, age) {
    this.age = age;
    this.name = name;
  }

  introduce() {
    let result = 'My name is ' + this.name + '. I am ' + this.age + ' years old.';
    return result;
  }
}

export default Person;
