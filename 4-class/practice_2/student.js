// TODO 11: 在这里写实现代码
import Person from './person';

class Student extends Person {
  constructor(name, age, classname) {
    super(name, age);
    this.klass = classname;
  }

  introduce() {
    let result = 'I am a Student. I am at Class ' + this.klass + '.';
    return result;
  }
}

export default Student;
