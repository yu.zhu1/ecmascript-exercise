// TODO 20: 在这里写实现代码
import Person from './person';

class Teacher extends Person {
  constructor(name, age, classname) {
    super(name, age);
    this.klass = classname;
  }

  introduce() {
    if (!this.klass) {
      return super.introduce() + ' I am a Teacher. I teach No Class.';
    }
    let result = super.introduce() + ' I am a Teacher. I teach Class ' + this.klass + '.';
    return result;
  }
}

export default Teacher;
