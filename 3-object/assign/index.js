export default function addSerialNumber(source) {
  // TODO 5: 在这里写实现代码，需要采用ES6 Object.assign
  let sN = { serialNumber: '12345' };
  let result = Object.assign(sN, source);
  result.properties.status = 'processed';
  return result;
}
