function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  let result = collection.filter(ele => ele % 3 == 0);
  return result;
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  let result = collection.filter((v, i, collection) => collection.indexOf(v) === i);
  return result;
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
